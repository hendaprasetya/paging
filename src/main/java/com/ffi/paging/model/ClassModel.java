/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.paging.model;

/**
 *
 * @author henda
 */
public class ClassModel {
    
    String userUpd = "";
    String dateUpd = "";
    String timeUpd = "";

    public String getUserUpd() {
        return userUpd;
    }

    public void setUserUpd(String userUpd) {
        this.userUpd = userUpd;
    }

    public String getDateUpd() {
        return dateUpd;
    }

    public void setDateUpd(String dateUpd) {
        this.dateUpd = dateUpd;
    }

    public String getTimeUpd() {
        return timeUpd;
    }

    public void setTimeUpd(String timeUpd) {
        this.timeUpd = timeUpd;
    }
}
