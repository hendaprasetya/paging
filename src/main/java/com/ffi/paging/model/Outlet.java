/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.paging.model;

/**
 *
 * @author henda
 */
public class Outlet extends ClassModel{
 
    String regionCode = "";
    String outletCode = "";
    String outletName = "";
    String type = "";
    String address1 = "";
    String address2 = "";
    String city = "";
    String postCode = "";
    String phone = "";
    String fax = "";
    Double cashBalance = 0d;
    String transDate = "";
    Double delLimit = 0d;
    Double delCharge = 0d;
    String rndPrint = "";
    Double rndFact = 0d;
    Double rndLimit = 0d;
    Double tax = 0d;
    Double dpMin = 0d;
    Double cancelFee = 0d;
    Double catItems = 0d;
    Double maxBills = 0d;
    Double minItems = 0d;
    Double refTime = 0d;
    Double timeOut = 0d;
    Double maxShift = 0d;
    String sendData = "";
    Double minPullTrx = 0d;
    Double maxPullValue = 0d;
    String status = "";
    String startDate = "";
    String finishDate = "";
    Double maxDiscPercent = 0d;
    Double maxDiscAmount = 0d;
    String openTime = "";
    String closeTIme = "";
    Double refundTimeLimit = 0d;
    String monday = "";
    String tuesday = "";
    String wednesday = "";
    String thursday = "";
    String friday = "";
    String saturday = "";
    String sunday = "";
    String holiday = "";
    String outlet24Hour = "";
    String ipOutlet = "";
    String portOutlet = "";
    String ftpAddr = "";
    String ftpUser = "";
    String ftpPassword = "";
    String initialOutlet = "";
    String areaCode = "";
    String rscCode = "";
    Double taxCharge = 0d;

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getOutletCode() {
        return outletCode;
    }

    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Double getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(Double cashBalance) {
        this.cashBalance = cashBalance;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public Double getDelLimit() {
        return delLimit;
    }

    public void setDelLimit(Double delLimit) {
        this.delLimit = delLimit;
    }

    public Double getDelCharge() {
        return delCharge;
    }

    public void setDelCharge(Double delCharge) {
        this.delCharge = delCharge;
    }

    public String getRndPrint() {
        return rndPrint;
    }

    public void setRndPrint(String rndPrint) {
        this.rndPrint = rndPrint;
    }

    public Double getRndFact() {
        return rndFact;
    }

    public void setRndFact(Double rndFact) {
        this.rndFact = rndFact;
    }

    public Double getRndLimit() {
        return rndLimit;
    }

    public void setRndLimit(Double rndLimit) {
        this.rndLimit = rndLimit;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getDpMin() {
        return dpMin;
    }

    public void setDpMin(Double dpMin) {
        this.dpMin = dpMin;
    }

    public Double getCancelFee() {
        return cancelFee;
    }

    public void setCancelFee(Double cancelFee) {
        this.cancelFee = cancelFee;
    }

    public Double getCatItems() {
        return catItems;
    }

    public void setCatItems(Double catItems) {
        this.catItems = catItems;
    }

    public Double getMaxBills() {
        return maxBills;
    }

    public void setMaxBills(Double maxBills) {
        this.maxBills = maxBills;
    }

    public Double getMinItems() {
        return minItems;
    }

    public void setMinItems(Double minItems) {
        this.minItems = minItems;
    }

    public Double getRefTime() {
        return refTime;
    }

    public void setRefTime(Double refTime) {
        this.refTime = refTime;
    }

    public Double getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Double timeOut) {
        this.timeOut = timeOut;
    }

    public Double getMaxShift() {
        return maxShift;
    }

    public void setMaxShift(Double maxShift) {
        this.maxShift = maxShift;
    }

    public String getSendData() {
        return sendData;
    }

    public void setSendData(String sendData) {
        this.sendData = sendData;
    }

    public Double getMinPullTrx() {
        return minPullTrx;
    }

    public void setMinPullTrx(Double minPullTrx) {
        this.minPullTrx = minPullTrx;
    }

    public Double getMaxPullValue() {
        return maxPullValue;
    }

    public void setMaxPullValue(Double maxPullValue) {
        this.maxPullValue = maxPullValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public Double getMaxDiscPercent() {
        return maxDiscPercent;
    }

    public void setMaxDiscPercent(Double maxDiscPercent) {
        this.maxDiscPercent = maxDiscPercent;
    }

    public Double getMaxDiscAmount() {
        return maxDiscAmount;
    }

    public void setMaxDiscAmount(Double maxDiscAmount) {
        this.maxDiscAmount = maxDiscAmount;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTIme() {
        return closeTIme;
    }

    public void setCloseTIme(String closeTIme) {
        this.closeTIme = closeTIme;
    }

    public Double getRefundTimeLimit() {
        return refundTimeLimit;
    }

    public void setRefundTimeLimit(Double refundTimeLimit) {
        this.refundTimeLimit = refundTimeLimit;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getOutlet24Hour() {
        return outlet24Hour;
    }

    public void setOutlet24Hour(String outlet24Hour) {
        this.outlet24Hour = outlet24Hour;
    }

    public String getIpOutlet() {
        return ipOutlet;
    }

    public void setIpOutlet(String ipOutlet) {
        this.ipOutlet = ipOutlet;
    }

    public String getPortOutlet() {
        return portOutlet;
    }

    public void setPortOutlet(String portOutlet) {
        this.portOutlet = portOutlet;
    }

    public String getFtpAddr() {
        return ftpAddr;
    }

    public void setFtpAddr(String ftpAddr) {
        this.ftpAddr = ftpAddr;
    }

    public String getFtpUser() {
        return ftpUser;
    }

    public void setFtpUser(String ftpUser) {
        this.ftpUser = ftpUser;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public String getInitialOutlet() {
        return initialOutlet;
    }

    public void setInitialOutlet(String initialOutlet) {
        this.initialOutlet = initialOutlet;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getRscCode() {
        return rscCode;
    }

    public void setRscCode(String rscCode) {
        this.rscCode = rscCode;
    }

    public Double getTaxCharge() {
        return taxCharge;
    }

    public void setTaxCharge(Double taxCharge) {
        this.taxCharge = taxCharge;
    }
    
}
